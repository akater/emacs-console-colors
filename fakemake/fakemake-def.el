;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'console-colors
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("console-colors-core"
                       "console-colors")
  site-lisp-config-prefix "50"
  license "GPL-3")

(defvar console-colors--colormapsdir
  (or (getenv "COLORMAPSDIR")
      "/usr/share/colormaps/"))

(defvar root)
(advice-add 'fakemake-install :after
            (lambda ()
              ;; insinto "${COLORMAPSDIR}"
	      ;; doins *.csv
	      ;; dosym material.csv "${COLORMAPSDIR}"dark
	      ;; dosym quasi-comaterial.csv "${COLORMAPSDIR}"light
              (with-filenames-relative-to root (console-colors--colormapsdir)
                (ensure-directory console-colors--colormapsdir)
                (do-default-directory-files (file (rx ".csv" string-end) nil t)
                  "Installed %s theme%s"
                  (copy-file file
                             (expand-file-name file
                                               console-colors--colormapsdir)))
                (make-symbolic-link "material.csv"
                                    (concat console-colors--colormapsdir
                                            "dark"))
                (make-symbolic-link "quasi-comaterial.csv"
                                    (concat console-colors--colormapsdir
                                            "light"))
                (message "Made standard symlinks"))))
